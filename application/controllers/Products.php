<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Products extends CI_Controller {

    public function __construct() {
        parent::__construct(); 
   
        $this->load->model('ProductsModel');
        $this->load->database();
     }

    public function index () {
        $data['rows']=$this->ProductsModel->getData();
        $this->load->view('products/lists',$data);
    }

    public function add_form () {
        $this->load->view('products/create');
    }

    public function create(){
        $this->input->post('submit');
		
        $data['name']=$this->input->post('name');
        $data['sku']=$this->input->post('sku');
        $data['unit']=$this->input->post('unit');
        $data['category']=$this->input->post('category');
        $data['brand']=$this->input->post('brand');
        $data['type']=$this->input->post('type');
        //var_dump ($data);
        //die();

        $last_id=$this->ProductsModel->insert_product($data);
        //echo $last_id;

        
        redirect('Products','refresh');
    }

    public function edit($id) {
		$data['products']=$this->ProductsModel->getProductbyID($id);
        $this->load->view('products/edit', $data);
    }

    public function update(){
		
        $id = $this->input->post('p_id');

    // var_dump($_POST);
    // die;

        $data=array(
            'name'=>$this->input->post('name'),
            'sku'=>$this->input->post('sku'),
            'unit'=>$this->input->post('unit'),
            'category'=>$this->input->post('category'),
            'brand'=>$this->input->post('brand'),
            'type'=>$this->input->post('type'),
        );

        $this->ProductsModel->update($data,$id);

        return redirect('products','refresh');

    }

    public function delete($id){

        $this->ProductsModel->destroy($id);
        return redirect('products','refresh');
    
   }

}