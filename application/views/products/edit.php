<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/nav'); ?>



<div class="card borderless-card">
<div class="card-block primary-breadcrumb">
<div class="breadcrumb-header">
<h5>Edit Products</h5>
</div>
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item" style="float: left;">
<a href="#!">
<i class="feather icon-home"></i>
</a>
</li>
<li class="breadcrumb-item" style="float: left;"><a href="#!">Products</a>
</li>
<li class="breadcrumb-item" style="float: left;"><a href="#!">Edit Products</a>
</li>
</ul>
</div>
</div>
</div>




<form method="post" action="<?php echo base_url('Products/update'); ?>">
<div class="form-group row">
<div class="col-sm-5">
<input type="text" class="form-control" name="name" value="<?php echo $products->name; ?>" placeholder="Product Name">
<input type="hidden" name="p_id" value="<?php echo $products->id; ?>" >
</div>
<div class="col-sm-3">
<input type="text" class="form-control" name="sku" value="<?php echo $products->sku; ?>"  placeholder="SKU">
</div>
<div class="col-sm-4">
<input type="text" class="form-control" name="unit" value="<?php echo $products->unit; ?>"  placeholder="Unit">
</div>
</div>
<div class="form-group row">
<div class="col-sm-4">
<input type="text" class="form-control" name="category" value="<?php echo $products->category; ?>"  placeholder="Category">
</div>
<div class="col-sm-4">
<input type="text" class="form-control" name="brand" value="<?php echo $products->brand; ?>"  placeholder="Brand">
</div>
<div class="col-sm-4">
<input type="text" class="form-control" name="type" value="<?php echo $products->type; ?>"  placeholder="Product Type">
</div>
</div>
<button class="btn btn-grd-primary" name="update" >Update</button>
</form>


<?php $this->load->view('layout/footer'); ?>