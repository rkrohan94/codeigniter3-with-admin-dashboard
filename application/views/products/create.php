<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/nav'); ?>



<div class="card borderless-card">
<div class="card-block primary-breadcrumb">
<div class="breadcrumb-header">
<h5>Create Products</h5>
</div>
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item" style="float: left;">
<a href="#!">
<i class="feather icon-home"></i>
</a>
</li>
<li class="breadcrumb-item" style="float: left;"><a href="#!">Products</a>
</li>
<li class="breadcrumb-item" style="float: left;"><a href="#!">Create Products</a>
</li>
</ul>
</div>
</div>
</div>





<form method="post" action="<?php echo base_url('products/create'); ?>">
<div class="form-group row">
<div class="col-sm-5">
<input type="text" class="form-control" name="name" placeholder="Product Name">
</div>
<div class="col-sm-3">
<input type="text" class="form-control" name="sku" placeholder="SKU">
</div>
<div class="col-sm-4">
<input type="text" class="form-control" name="unit" placeholder="Unit">
</div>
</div>
<div class="form-group row">
<div class="col-sm-4">
<input type="text" class="form-control" name="category" placeholder="Category">
</div>
<div class="col-sm-4">
<input type="text" class="form-control" name="brand" placeholder="Brand">
</div>
<div class="col-sm-4">
<input type="text" class="form-control" name="type" placeholder="Product Type">
</div>
</div>
<button class="btn btn-grd-primary" name="submit" >Submit</button>
</form>


<?php $this->load->view('layout/footer'); ?>