<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/nav'); ?>


<div class="card borderless-card">
    <div class="card-block primary-breadcrumb">
        <div class="breadcrumb-header">
            <h5>List Products</h5>
        </div>
        <div class="page-header-breadcrumb">
            <a href="<?php echo base_url('products/add_form')?>" class="btn btn-success btn-out-dashed pull-left" role="button">Add New Product</a>
        </div>
    </div>
</div>



<div class="card">

<div class="card-block">

<div class="table-responsive dt-responsive">

<div id="dom-jqry_wrapper" class="dataTables_wrapper dt-bootstrap4">    

            

      <div class="row">
          <div class="col-xs-12 col-sm-12">     
        <table id="dom-jqry" class="table table-striped table-bordered nowrap dataTable" style="width: 100%;" role="grid" aria-describedby="dom-jqry_info">
            
           
<thead>
<tr role="row">
    <th class="sorting_asc" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 259px;" aria-sort="ascending" >NAME</th>
    <th class="sorting" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 398px;" >SKU</th>
    <th class="sorting" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 202px;" >UNIT</th>
    <th class="sorting" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 105px;" >CATEGORY</th>
    <th class="sorting" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 191px;" >BRAND</th>
    <th class="sorting" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 138px;" >PRODUCT TYPE</th>
    <th class="sorting" tabindex="0" aria-controls="dom-jqry" rowspan="1" colspan="1" style="width: 138px;" ></th>
</tr>
</thead>



<tbody>
 <?php foreach ($rows as $row) : ?>
     
     
<tr role="row" class="odd">
<td class="sorting_1"><?php echo $row->name; ?></td>
<td><?php echo $row->sku; ?></td>
<td><?php echo $row->unit; ?></td>
<td><?php echo $row->category; ?></td>
<td><?php echo $row->brand; ?></td>
<td><?php echo $row->type; ?></td>
<td style="white-space: nowrap; width: 1%;">
    <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
        <div class="btn-group btn-group-sm" style="float: none;">
            <a href="<?php echo base_url('products/edit/'.$row->id); ?>" class="btn btn-primary" style="float: none;margin: 5px;">
                <span class="icofont icofont-ui-edit"></span>
            </a>
            <a href="<?php echo base_url('products/delete/'.$row->id); ?>" class="btn btn-danger" style="float: none;margin: 5px;">
                <span class="icofont icofont-ui-delete"></span>
            </a>
        </div>
    </div>
</td>
</tr>
<?php endforeach;?>
</tbody>

</table>
</div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-5"><div class="dataTables_info" id="dom-jqry_info" role="status" aria-live="polite">Showing 1 to 10 of 20 entries</div></div>
    <div class="col-xs-12 col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="dom-jqry_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="dom-jqry_previous"><a href="#" aria-controls="dom-jqry" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="dom-jqry" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="dom-jqry" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item next" id="dom-jqry_next"><a href="#" aria-controls="dom-jqry" data-dt-idx="3" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
<br>
</div>
</div>
</div>


<?php $this->load->view('layout/footer'); ?>