

<nav class="pcoded-navbar">
<div class="pcoded-inner-navbar main-menu">
    


<div class="pcoded-navigatio-lavel">Dashboard</div>
<ul class="pcoded-item pcoded-left-item">
<li class="">
<a href="#">
<span class="pcoded-micon"><i class="feather icon-home"></i></span>
<span class="pcoded-mtext">Home</span>
</a>

</li>
</ul>

<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu pcoded-trigger">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="feather icon-home"></i></span>
<span class="pcoded-mtext">Products</span>
</a>

<ul class="pcoded-submenu">
<li class="">
<a href="<?php echo base_url('products/index'); ?>">
<span class="pcoded-mtext">Lists Products</span>
</a>
</li>
</ul>

<ul class="pcoded-submenu">
<li class="">
<a href="<?php echo base_url('products/add_form'); ?>">
<span class="pcoded-mtext">Create Products</span>
</a>
</li>
</ul>

</li>
</ul>

<div class="pcoded-navigatio-lavel">Support</div>
<ul class="pcoded-item pcoded-left-item">
<li class="">
<a href="#">
<span class="pcoded-micon"><i class="feather icon-monitor"></i></span>
<span class="pcoded-mtext">Documentation</span>
</a>
</li>
<li class="">
<a href="#">
<span class="pcoded-micon"><i class="feather icon-help-circle"></i></span>
<span class="pcoded-mtext">Submit Issue</span>
</a>
</li>
</ul>
</div>
</nav>







<!--Page Content Start Here -->

<div class="pcoded-content">
<div class="pcoded-inner-content">
<div class="main-body">
<div class="page-wrapper">
<div class="page-body">
