<?php
class ProductsModel extends CI_Model{
    

    public function getData() {

        //$this->db->select('*');
        //$this->db->from('products');
        $query=$this->db->get('products'); //$query=$this->db->get();
        return $query->result();

        }
    
    public function insert_product($data) {
        $this->db->insert('products', $data);
        //return true;
        return $this->db->insert_id();
    }

    public function getProductbyID($id) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('id',$id);

        return $this->db->get()->row();
    }

    public function update( $data, $id) {
        // echo "<pre>";
        // print_r($data);
        // exit;
        $this->db->where('id', $id);
        $this->db->update('products', $data);
        
        
        // $str = $this->db->last_query();
        // echo "<pre>";
        // print_r($str);
        // exit;
        
    }

    public function destroy($id)
    {
        
        $this->db->where('id', $id);
        $this->db->delete('products');
        return TRUE;
        // $str = $this->db->last_query();
        // echo "<pre>";
        // print_r($str);
         //exit;
    }

}